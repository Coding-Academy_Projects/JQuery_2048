(function($)
{
    $.fn.game_2048 = function()
    {
        var table = $("<table></table>").attr({id: "grid"});
        var score_record = 0;
        var new_game = false;
        var move_done = false;

        create_table();
        score();

        // Start game by pressing "New game" button
        $(".new_game").click(function()
        {
            start();
        })

        // Start game by pressing enter
        $("body").keypress(function (e)
        {
            if (e.keyCode == 13)
            {
                start();
            }
        });

        // All functions to start a new game
        function start()
        {
            reset_merge_attr();
            table.empty();
            create_table();
            score_record = 0;
            score();
            random_number(2);
            colour();
            new_game = true; 
        }

        // Function to create a table
        function create_table()
        {
            var id_count = 1;

            for(var y = 0; y < 4; y++)
            {
                var row = $("<tr></tr>").appendTo(table);
                for (var x = 0; x < 4; x++)
                {
                    var cell = $("<td></td>").attr("x", x).attr("y", y);
                    cell.text("0");
                    cell.attr({id: id_count})
                    row.append(cell);
                    id_count++;
                }
            }
            $(".canvas").append(table);
        }

        // Score display
        function score()
        {
            $(".score").text("Score:  "+ score_record);
        }

        // Function to know where are the empty cells (cells contening 0)
        function search_zero()
        {
            var count = 0;

            for (var y = 0; y < 4; y++)
            {
                for (var x = 0; x < 4; x++)
                {
                    var pos = $('[x="' + x +'"][y="' + y + '"]');

                    if (pos.text() === "0")
                    {
                        count++;
                        return true;
                    }
                }
            }
            return false;
        }

        // Generate a random number in an empty cell if it's possible
        function random_number(nbr)
        {
            while (nbr > 0)
            {
                var chosen_cell = Math.floor(Math.random() * 17);
                var chosen_number = Math.random() < 0.7 ? 2 : 1024;
                var search = search_zero();

                if (search == true)
                {
                    if ($('#' + chosen_cell).text() == "0")
                    {
                        $('#' + chosen_cell).text(chosen_number);
                        nbr--;
                    }
                    else
                    {
                        random_number(nbr);
                        return;
                    }
                }
                
                search = search_zero();
                var merge = check_merge();
                
                if (search == false)
                {
                    if (merge == false)
                    {
                        new_game = false;
                        create_loss_table();
                    }
                }
            }
        }

        // Check if a merge is possible between numbers
        function check_merge()
        {
            var count = 0;

            for (var y = 0; y < 4; y++)
            {
                for (var x = 0; x < 4; x++)
                {
                    var pos = $('[x="' + x +'"][y="' + y + '"]');
                    var right_pos = $('[x="' + (x+1) +'"][y="' + y + '"]');
                    var bottom_pos = $('[x="' + x +'"][y="' + (y+1) + '"]');

                    if (pos.text() == right_pos.text() || pos.text() == bottom_pos.text())
                    {
                        count++;
                        return true;
                    }
                }
            }
            return false;
        }

        // Reset merge flag (flag to avoid multiple merge at the same time)
        function reset_merge_attr()
        {
            for (var y = 0; y < 4; y++)
            {
                for (var x = 0; x < 4; x++)
                {
                    var pos = $('[x="' + x +'"][y="' + y + '"]');
                    pos.attr("merge", 0);
                }
            }
        }
        
        // After move if game isn't finished
        function after_move()
        {
            if (new_game == true)
            {
                score();
                 if (move_done == true)
                 {
                     random_number(1);
                     move_done = false;
                 }
                 colour();
                 reset_merge_attr();
            }
        }
        // Arrow keys functions
        $(document).keydown(function(e)
        {
            if (e.keyCode === 37)
            {
                if (new_game == true)
                {
                    moveLeft();
                    after_move();
                }
            }

            else if (e.keyCode === 38)
            {
                if (new_game == true)
                {
                    moveUp();
                    after_move();
                }
            }
            else if (e.keyCode === 39)
            {
                if (new_game == true)
                {
                    moveRight();
                    after_move();
                }
            }
            else if (e.keyCode === 40)
            {
                if (new_game == true)
                {
                    moveDown();
                    after_move();
                }
            }
        });

        // Left
        function moveLeft()
        {            
            var fixed_pos = null;

            for (var y = 0; y < 4; y++)
            {
                for (var x = 0; x < 4; x++)
                {
                    var pos = $('[x="' + x +'"][y="' + y + '"]');
                       
                    if (pos.text() == 0 && fixed_pos == null)
                    {
                        fixed_pos = $('[x="' + x +'"][y="' + y + '"]');
                    }
                    else if (pos.text() != 0 && fixed_pos != null)
                    {
                        fixed_pos.text(pos.text());
                        pos.text(0);

                        if (fixed_pos.attr("x") > 0)
                        {
                            x = parseInt(fixed_pos.attr("x")) - 1;

                            var left_pos = $('[x="' + x +'"][y="' + y + '"]');
                            var int_left = parseInt(left_pos.text());
                            var int_pos = parseInt(fixed_pos.text());

                            if (int_left == int_pos && left_pos.attr("merge") == "0")
                            {
                                int_left += int_pos;
                                left_pos.text(int_left);
                                if (left_pos.text() == "2048")
                                {
                                    new_game = false;
                                    create_win_table();
                                    return true;
                                }
                                else
                                {
                                    left_pos.attr("merge", 1);
                                    score_record += int_left;
                                    fixed_pos.text(0);
                                }
                            }
                        }
                        x = fixed_pos.attr("x");
                        fixed_pos = null;
                        move_done = true;
                    }
                    else if (pos.text() != 0 && fixed_pos == null)
                    {
                        if (x < 3)
                        {
                            var right_pos = $('[x="' + (x+1) +'"][y="' + y + '"]');
                            var int_right = parseInt(right_pos.text());
                            var int_pos = parseInt(pos.text());

                            if (int_pos == int_right && pos.attr("merge") == "0")
                            {
                                int_pos += int_right;
                                pos.text(int_pos);
                                if (pos.text() == "2048")
                                {
                                    new_game = false;
                                    create_win_table();
                                    return true;
                                }
                                else
                                {
                                    pos.attr("merge", 1);
                                    score_record += int_pos;
                                    right_pos.text(0);
                                    move_done = true;
                                }
                            }
                        }
                    }
                }
                fixed_pos = null;
            }          
        }

        // Up
        function moveUp()
        {
            var fixed_pos = null;

            for (var x = 0; x < 4; x++)
            {
                for (var y = 0; y < 4; y++)
                {
                    var pos = $('[x="' + x +'"][y="' + y + '"]');

                    if (pos.text() == 0 && fixed_pos == null)
                    {
                        fixed_pos = $('[x="' + x +'"][y="' + y + '"]');
                    }
                    else if (pos.text() != 0 && fixed_pos != null)
                    {
                        fixed_pos.text(pos.text());
                        pos.text(0);

                        if (fixed_pos.attr("y") < 3)
                        {
                            y = parseInt(fixed_pos.attr("y")) - 1;

                            var up_pos = $('[x="' + x +'"][y="' + y + '"]');
                            var int_up = parseInt(up_pos.text());
                            var int_pos = parseInt(fixed_pos.text());

                            if (int_up == int_pos && up_pos.attr("merge") == "0")
                            {
                                int_up += int_pos;
                                up_pos.text(int_up);
                                if (up_pos.text() == "2048")
                                {
                                    new_game = false;
                                    create_win_table();
                                    return true;
                                }
                                else
                                {
                                    up_pos.attr("merge", 1);
                                    score_record += int_up;
                                    fixed_pos.text(0);
                                }
                            }
                        }
                        y = fixed_pos.attr("y");
                        fixed_pos = null;
                        move_done = true;
                    }
                    else if (pos.text() != 0 && fixed_pos == null)
                    {
                        if (y < 3)
                        {
                            var bottom_pos = $('[x="' + x +'"][y="' + (y+1) + '"]');
                            var int_bottom = parseInt(bottom_pos.text());
                            var int_pos = parseInt(pos.text());

                            if (int_pos == int_bottom && pos.attr("merge") == "0")
                            {
                                int_pos += int_bottom;
                                pos.text(int_pos);
                                if (pos.text() == "2048")
                                {
                                    new_game = false;
                                    create_win_table();
                                    return true;
                                }
                                else
                                {
                                    pos.attr("merge", 1);
                                    score_record += int_pos;
                                    bottom_pos.text(0);
                                    move_done = true;
                                }
                            }
                        }
                    }
                }
                fixed_pos = null;
            } 
        }

        // Right
        function moveRight()
        {
            var fixed_pos = null;

            for (var y = 0; y < 4; y++)
            {
                for (var x = 3; x >= 0; x--)
                {
                    var pos = $('[x="' + x +'"][y="' + y + '"]');

                    if (pos.text() == 0 && fixed_pos == null)
                    {
                        fixed_pos = $('[x="' + x +'"][y="' + y + '"]');
                    }
                    else if (pos.text() != 0 && fixed_pos != null)
                    {
                        fixed_pos.text(pos.text());
                        pos.text(0);

                        if (fixed_pos.attr("x") < 3)
                        {
                            x = parseInt(fixed_pos.attr("x")) + 1;

                            var right_pos = $('[x="' + x +'"][y="' + y + '"]');
                            var int_right = parseInt(right_pos.text());
                            var int_pos = parseInt(fixed_pos.text());

                            if (int_right == int_pos && right_pos.attr("merge") == "0")
                            {
                                int_right += int_pos;
                                right_pos.text(int_right);
                                if (right_pos.text() == "2048")
                                {
                                    new_game = false;
                                    create_win_table();
                                    return true;
                                }
                                else
                                {
                                    right_pos.attr("merge", 1);
                                    score_record += int_right;
                                    fixed_pos.text(0);
                                }
                            }
                        }
                        x = fixed_pos.attr("x");
                        fixed_pos = null;
                        move_done = true;
                    }
                    else if (pos.text() != 0 && fixed_pos == null)
                    {
                        if (x > 0)
                        {
                            var left_pos = $('[x="' + (x-1) +'"][y="' + y + '"]');
                            var int_left = parseInt(left_pos.text());
                            var int_pos = parseInt(pos.text());

                            if (int_pos == int_left && pos.attr("merge") == "0")
                            {
                                int_pos += int_left;
                                pos.text(int_pos);
                                if (pos.text() == "2048")
                                {
                                    new_game = false;
                                    create_win_table();
                                    return true;
                                }
                                else
                                {
                                    pos.attr("merge", 1);
                                    score_record += int_pos;
                                    left_pos.text(0);
                                    move_done = true;
                                }
                            }
                        }
                    }
                }
                fixed_pos = null;
            }
        }

        // Down
        function moveDown()
        {
            var fixed_pos = null;

            for (var x = 0; x < 4; x++)
            {
                for (var y = 3; y >= 0; y--)
                {
                    var pos = $('[x="' + x +'"][y="' + y + '"]');

                    if (pos.text() == 0 && fixed_pos == null)
                    {
                        fixed_pos = $('[x="' + x +'"][y="' + y + '"]');
                    }
                    else if (pos.text() != 0 && fixed_pos != null)
                    {
                        fixed_pos.text(pos.text());
                        pos.text(0);

                        if (fixed_pos.attr("y") > 0)
                        {
                            y = parseInt(fixed_pos.attr("y")) + 1;

                            var down_pos = $('[x="' + x +'"][y="' + y + '"]');
                            var int_down = parseInt(down_pos.text());
                            var int_pos = parseInt(fixed_pos.text());

                            if (int_down == int_pos && down_pos.attr("merge") == "0")
                            {
                                int_down += int_pos;
                                down_pos.text(int_down);
                                if (down_pos.text() == "2048")
                                {
                                    new_game = false;
                                    create_win_table();
                                    return true;
                                }
                                else
                                {
                                    down_pos.attr("merge", 1);
                                    score_record += int_down;
                                    fixed_pos.text(0);
                                }
                            }
                        }
                        y = fixed_pos.attr("y");
                        fixed_pos = null;
                        move_done = true;
                    }
                    else if (pos.text() != 0 && fixed_pos == null)
                    {
                        if (y > 0)
                        {
                            var up_pos = $('[x="' + x +'"][y="' + (y-1) + '"]');
                            var int_up= parseInt(up_pos.text());
                            var int_pos = parseInt(pos.text());

                            if (int_pos == int_up && pos.attr("merge") == "0")
                            {
                                int_pos += int_up;
                                pos.text(int_pos);
                                if (pos.text() == "2048")
                                {
                                    new_game = false;
                                    create_win_table();
                                    return true;
                                }
                                else
                                {
                                    pos.attr("merge", 1);
                                    score_record += int_pos;
                                    up_pos.text(0);
                                    move_done = true;
                                }
                            }
                        }
                    }
                }
                fixed_pos = null;
            }
        }

        // Erase current table to display a winning table
        function create_win_table()
        {
            table.empty();
            create_table();
            $("td").css({"font-size": "70px", "color": "#FFFFFF"});
            
            for (var i = 1; i < 17; i++)
            {
                if (i == 1 || i == 2 || i == 3 || i == 4)
                {
                    $("#" + i).text("").css("background-color", "#760F98");;
                }
                else if (i == 13 || i == 14 || i == 15 || i == 16)
                {
                    {
                        $("#" + i).text("").css("background-color", "#fdb361");
                    }
                }
                else if (i == 5)
                {
                    $("#5").text("W").css("background-color", "#E13F5B");
                }
                else if (i == 6)
                {
                    $("#6").text("E").css("background-color", "#E13F5B");
                }
                else if (i == 7 || i == 8)
                {
                    $("#" + i).text("L").css("background-color", "#E13F5B");
                }
                else if (i == 9)
                {
                    $("#9").text("D").css("background-color", "#F07A52");
                }
                else if (i == 10)
                {
                    $("#10").text("O").css("background-color", "#F07A52");
                }
                else if (i == 11)
                {
                    $("#11").text("N").css("background-color", "#F07A52");
                }
                else if (i == 12)
                {
                    $("#12").text("E").css("background-color", "#F07A52");
                }
            }
        }

        // Erase current table to display a losing table
        function create_loss_table()
        {
            table.empty();
            create_table();
            $("td").css({
                "background-color": "#e13f5b",
                "color": "#FFFFFF",
                "font-size": "70px"
            });
            
            for (var i = 1; i < 17; i++)
            {
                if (i == 1 || i == 2 || i == 3 || i == 4 || i == 13 ||
                    i == 14 || i == 15 || i == 16)
                {
                    $("#" + i).text("");
                }
                else if (i == 5)
                {
                    $("#5").text("G");
                }
                else if ( i == 6)
                {
                    $("#6").text("A");
                }
                else if (i == 7)
                {
                    $("#7").text("M");
                }
                else if (i == 8 || i == 11)
                {
                    $("#8").text("E");
                    $("#11").text("E");
                }
                else if (i == 9)
                {
                    $("#9").text("O");
                }
                else if (i == 10)
                {
                    $("#10").text("V");
                }
                else if (i == 12)
                {
                    $("#12").text("R");
                }
            }
        }

        // Display different colours for each number
        function colour()
        {
            $("td").each(function()
            {
                switch ($(this).text())
                {
                    case "0":
                        $(this).css({'background-color' : '#FEE6CA', 'color' : '#FEE6CA'});
                        break;
                    case "2":
                        $(this).css({'background-color' : '#FEE097', 'color' : '#FFFFFF'});
                        break;
                    case "4":
                        $(this).css({'background-color': '#FED964', 'color' : '#FFFFFF'});
                        break;
                    case "8":
                        $(this).css({'background-color': '#fdb361', 'color' : '#FFFFFF'});
                        break;
                    case "16":
                        $(this).css({'background-color': '#F7975A', 'color' : '#FFFFFF'});
                        break;
                    case "32":
                        $(this).css({'background-color': '#f07a52', 'color' : '#FFFFFF'});
                        break;
                    case "64":
                        $(this).css({'background-color': '#e13f5b', 'color' : '#FFFFFF'});
                        break;
                    case "128":
                        $(this).css({'background-color': '#BB226A', 'color' : '#FFFFFF'});
                        break;
                    case "256":
                        $(this).css({'background-color': '#991981', 'color' : '#FFFFFF'});
                        break;
                    case "512":
                        $(this).css({'background-color': '#760f98', 'color' : '#FFFFFF'});
                        break;
                    case "1024":
                        $(this).css({'background-color': '#3B084C', 'color' : '#FFFFFF'});
                        break;
                    case "2048":
                        $(this).css({'background-color': '#000000', 'color' : '#FFFFFF'});
                        break;
                }
            })
        }
    };
}(jQuery));